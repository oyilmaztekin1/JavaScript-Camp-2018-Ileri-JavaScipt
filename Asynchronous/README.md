
# Asenkron Programlama


## Program Akışı

![enter image description here](https://mdn.mozillademos.org/files/4617/default.svg)


## Thread

 1. Single Thread
 2. Multi Thread (  concurrency - eşzamanlılık )

**Single Thread**
1. Aynı anda sadece bir işlem gerçekleşebilir
2. Bir işlem bitmeden diğerine geçilemez

**Multi Thread**
1. Aynı anda birden fazla işlem gerçekleşebilir
2. Bir işlem bitmeden diğeri başlayabilir. ( paralel işlemler )

**Asenkron**
1. Main flow dan bağımsız çalışır
2. Araya kaynak yapar ( flow u durdurur )
3. Callback fonksiyona ihtiyaç duyar  

```javascript
console.log('1')
setTimeout(function afterTwoSeconds() {
  console.log('2')
}, 2000)
console.log('3')
```

**Bu kod nasıl çalışır?**
```js
function waitThreeSeconds(){
	var ms = 3000 + new Date().getTime();
	while(new Date() < ms){}
	console.log('finished function');
}

waitThreeSeconds();
console.log('finsihed execution');
```

## CORS & HTTP Requests
![enter image description here](https://mdn.mozillademos.org/files/14295/CORS_principle.png)
-  Sayfayı yenilemeden veri güncelleme
-  Sunucuya istek atma (HTTP Request ) 
-  Sunucudan veri alma ( GET)
-  Sunucuya veri gönderme - (POST)
-  HTML rendering yok

**Yöntemler**
1. **XMLHTTPRequest**
```javascript
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       document.getElementById("demo").innerHTML = xhttp.responseText;
    }
};
xhttp.open("GET", "url", true);
xhttp.send();
```
2. **Fetch**
- Error Handling problemi
```js
fetch('http://example.com/movies.json')
  .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
    console.log(myJson);
  });
```

3. Axios
```js
axios.get('/user?ID=12345')
  .then(function (response) {
    // handle success
    console.log(response);
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .then(function () {
    // always executed
  });
```
**Promise**
1. Promise zinciri 
2. Daha efektif callback yönetimi yapılabilir
3. Daha güçlü hata yönetimi  


```js
function doubleAfter2Seconds(x) {  
  return new Promise(resolve => {  
    setTimeout(() => {  
      resolve(x * 2);  
    }, 2000);  
  });  
}

doubleAfter2Seconds(10).then((r) => {  
  console.log(r);  
});
```


**Promise Chain**
```js
function doubleAfter2Seconds(x) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(x * 2);
    }, 2000);
  });
}

function addPromise(x){
  return new Promise(resolve => {
    doubleAfter2Seconds(10).then((a) => {
      doubleAfter2Seconds(20).then((b) => {
        doubleAfter2Seconds(30).then((c) => {
          resolve(x + a + b + c);
      	})
      })
    })
  });
}

addPromise(10).then((sum) => {
  console.log(sum);
});
```

**Async**
Avantaj

 1. Performansı arttırır. Event Loop daha efektif çalışır. ???
 2. Daha organize bir kod
 3. Promise döndürür
 4. Await ile durdurulabilir

```js
function doubleAfter2Seconds(x) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(x * 2);
    }, 2000);
  });
}

async function addAsync(x) {
  const a = await doubleAfter2Seconds(10);
  const b = await doubleAfter2Seconds(20);
  const c = await doubleAfter2Seconds(30);
  return x + a + b + c;
}


addAsync(10).then((sum) => {
  console.log(sum);
});
```

**ne yaptık**
1. `addAsync` ı 10 parametresiyle invoke ettik
2. ` a` devreye girdi fonksiyon iki saniyeliğine durdu ve resolve bastı. (normalde araya girer)
3. 6 saniye sonra `console.log(sum)` döndürdük.



### Kaynaklar
http://exploringjs.com/es6/ch_async.html

https://eloquentjavascript.net/11_async.html

https://stackoverflow.com/questions/748175/asynchronous-vs-synchronous-execution-what-does-it-really-mean

https://stackoverflow.com/questions/8963209/does-async-programming-mean-multi-threading

https://stackify.com/when-to-use-asynchronous-programming/

https://codeburst.io/javascript-es-2017-learn-async-await-by-example-48acc58bad65


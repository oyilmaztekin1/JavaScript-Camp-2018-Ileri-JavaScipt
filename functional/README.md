

# Fonksiyonel Programlama
Yan etkisi yoktur.
Sadece girdi ve çıktı vardır.
Mutasyon ihtimali daha azdır.
Aynı girdi verildiğinde aynı çıktıyı verir.
Sıklıkla `Callback` kullanılır.
`Closure`vazgeçilmezdir.

## High-Order & First-Class Functions
!!! Fonksiyonlar birinci sınıf vatandaştır. First-class citizen
1- Fonksiyonlar fonksiyonları argüman olarak kabul eder.
2- Fonksiyon başka bir fonksiyonu döndürebilir
3- Fonksiyon değişkene eşitenebilir
4- Veri yapıları içerisinde tutulabilir. (array, obje gibi...)
5- Anonim fonksiyonlar

## Imperative vs declarstive
```javascript
    //imperative
    x => x * x 
    
    (function(x){
        return x * x
    })(5)

    //declerative

    let hsp = x => x * x
    hsp(3) // 6

    let hsp2 = function(x){
        return x * x;
    }
```

## callback
Bir fonksiyon bittiğinde başlayan fonksiyon

**jQuery Örneği**
```js
$('#element').show('slow', function() {
    // callback function
});
```
**Pure javascript**
```js
a("Hello world", function(s) {
	console.log(s + ", how are you?");
});

function a(s, callback) {
	callback(s);
}
```

```js
a("Hello world", b);

function a(s, callback) {
	callback(s + ' canım');
}
```
## Map
```javascript
Math.max(2, 8, 5); // 8
```

En iyi Mapleme örnekleri için
https://developer.mozilla.org/tr/docs/Web/JavaScript/Reference/Global_Objects/Array/filter#

## Currying

 1. Parametreler parça parça işleme solulur
 2. Her dış fonksiyon içe, işlenmiş parametreyi devreder
 3. İçerde değişenler dışarıdaki işlemi etkilemez 

```javascript 
function sum3(x) {
  return function(y) {
    return function(z) {
      return x + y + z;
    };
  };
}
console.log(sum3(1)(2)(3)) // 6
```

**Ayrı ayrı işleme sokma**
```js
function sum3(x) {
   let inputX = x + x; 
   return function(y) {
       let inputY = y + y;
       return function(z){
           let inputZ = z + z
           return inputX + inputY + inputZ;
       }
   }
}
console.log(sum3(1)(2)(3)) // 12
```

## Closure
 Fonksiyon kendi ortamıyla beraber tutulur.
![enter image description here](http://prashantb.me/content/images/2017/01/js_runtime.png)

```js
function greet(whattosay) {

   return function(name) {
       console.log(whattosay + ' ' + name);
   }

}
greet('Selam')('Özer');
```

## Recursion
Fonksiyonun kendi kendini içeriden çağırması işlemidir.

**Fibonacci Dizimi** işlemi en iyi recursive örnektir.
`fn(n) => fn(n-1) + fn(n+2)``

```javascript
function fib(n){
	return n == 1 ? 0:
	       n == 2 ? 1:
               fib(n-1) + fib(n-2);
}
```


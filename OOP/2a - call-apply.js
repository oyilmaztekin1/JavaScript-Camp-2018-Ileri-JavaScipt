function speak(line) {
  console.log(`The ${this.type} rabbit says '${line}'`);
}

let hungryRabbit = {type: "hungry", speak};

hungryRabbit.speak("I could use a carrot right now.");
// → The hungry rabbit says 'I could use a carrot right now.'

speak.call(hungryRabbit, "Burp!");
speak.apply(hungryRabbit, ["Gurr Gurr!"]);

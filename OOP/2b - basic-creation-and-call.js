function Person(firstname, lastname) {
  this.firstname = firstname || 'Default';
  this.lastname = lastname || 'Default2';
  this.getFullName = function(firstname, lastname) {
        return this.firstname + ' ' + this.lastname;  
    }
}

var jane = new Person('jane');

var getFormalFullName = function() {
  return this.lastname + ', ' + this.firstname;   
}

Person.prototype.getFormalFullName = getFormalFullName;

Person.prototype.getFullNameProto = function() {
    return this.firstname + ' ' + this.lastname;  
}

var john = {
    firstname: 'John',
    lastname: 'Doe'
}

console.log(jane.getFullName());
console.log(jane.getFormalFullName.call(john));

// Asla kullanma ( Performans Problemi)
// john.__proto__ = jane;
// console.log(john.getFullName())

// function Person2(firstname, lastname){
//   this.firstname = firstname
//   this.lastname = lastname
// }

// var person2 = new Person2('prototype','cemil');

// Person2.prototype = Person.prototype

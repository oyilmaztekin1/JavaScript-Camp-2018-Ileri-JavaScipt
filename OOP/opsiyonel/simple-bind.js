// The bind() method creates a new function that, when called, 
// has its this keyword set to the provided value, 
// with a given sequence of arguments preceding any provided when the new function is called.

function multiply(num1, num2) {
  return num1 * num2;
}
// multiply(1, 3); // 3
var doubler = multiply.bind(this, 2);
doubler(3); // 6

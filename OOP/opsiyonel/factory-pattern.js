function Person(firstname,lastname){
    var obj = {}
    obj.firstname = firstname
    obj.lastname = lastname
    obj.getFullName = function() {
        return this.firstname + ' ' + this.lastname;
    }
    return obj;
}

var ozer = Person('ali','veli');
console.log(ozer.getFullName());

function ConstructApple(color,width,height){
  this.color = color;
  this.width = width;
  this.height = height;
 
}

ConstructApple.prototype = {
  makeItBlue: function(){return this.color = "blue";},
  makeItBigger: function(){return this.height = this.height * 2;}
}

var apple = new ConstructApple("green",100,300)
var apple2 = new ConstructApple("red",200,400)

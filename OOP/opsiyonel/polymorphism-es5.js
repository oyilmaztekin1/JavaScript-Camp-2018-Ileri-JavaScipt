// Parent Constructor

function Parent( p1, p2 ) {
    var prop1 = p1;		// private property
    this.prop2 = p2;	// public property
    // private method
    function meth1() { return prop1; }
    // public methods
    this.meth2 = function(){ return this.prop2; };
    this.meth3 = function(){ return meth1(); };
}

var pc = new Parent( "one", "two" );	// create new Parent object


function Child( p1, p2, p3, p4 ) {
    Parent.apply( this, arguments ); // initialize Parent's members
    this.prop3 = p3;
    this.meth3 = function(){ return this.prop3; }	// override
    var prop4 = p4;
    this.meth4 = function(){ return prop4; }
}
Child.prototype = new Parent();	// set up inheritance relation

var cc = new Child( "one", "two", "three", "four" );

var result1 = cc.prop2;		// parent property via child
var result2 = cc.meth2();	// parent method via child
var result3 = cc.meth3();	// child method overrides parent method
var result4 = cc.meth4();	// child method

var cc2 = new Child( "one", "two", "three", "four"); // another child
cc2.prop2 = "something else";	// change other child’s parent’s member


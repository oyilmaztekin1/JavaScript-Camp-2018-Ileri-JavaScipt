# Prototype

### classical vs. prototypal inharitance
**inharitance:** Bir objenin başka bir objenin özellik veya metodlarına erişmesidir. (Katılım)

***classical***: java, C#, etc... 
parent-child yukarıdan aşağıya keskin kalıtsal ilişki

![enter image description here](https://image.slidesharecdn.com/inheritancecontinues-1223274749680392-9/95/c-inheritance-composition-polymorphism-42-728.jpg?cb=1332142453)

***prototypal:*** 
Ağaç şeklinde, paylaştırılabilir ilişki.
esnek, genişletilebilir, esnetilebilir, anlaşılması daha kolay.

![enter image description here](https://4.bp.blogspot.com/-ZaGWJf5fHcU/UGs_P61HOGI/AAAAAAAAAew/Kzl96APzqlk/s640/Javascript+Prototypal+Inheritance+Diagram+-+grand+picture+-+with+some+Dog+objects+%281%29.png)


Prototip bir objenin referans olarak kullandığı başka bir objedir.

Sadece constructor fonksiyonların **prototipi** bulunur. Prototype sadece fonksiyonda çalışır.

Object de bir fonksiyondur. New olmadan çağırdığında fonksiyonu invoke eder. Bu yüzden objenin constructor özelliğinin prototipi bulunur.  ``` Object.__proto__ ``` boştur. ```Object.prototype``` constructor prototiplerini döker. 

```javascript
let obj = {}
obj.prototype // undefined
obj.__proto__ // all property
obj.constructor.prototype // all prototype
Object.prototype
Object.__proto__
```
***__proto__:***  objenin özellikleri (properties)
***prototype:*** constructor fonksiyona ait built-in properties-methods ```Array.prototype.*``` gibi...

```javascript
function Foo(name)  {
	this.name  =  name;
}

class Foo {
	constructor(name) {
		this.name = name
	}
}
```

yeni class keyword ünde new olmadan fonksiyon invoke edilemez. ```Uncaught TypeError: Class constructor Foo cannot be invoked without 'new'```


![enter image description here](http://www.javascripttutorial.net/wp-content/uploads/2016/09/JavaScript-Prototype-300x109.png)

 1. Foo isminde bir fonksiyon yaratıldı. 
 2. Fonksiyon için anonim bir obje yaratıldı.
 3. Anonim objenin constructor özelliği Foo fonksiyonunu gösterdi

 
```javascript
console.log(foo.prototype) // undefined
console.log(foo.__proto__) // constructor
console.log(foo.constructor.prototype) // constructor properties
```

![enter image description here](http://www.javascripttutorial.net/wp-content/uploads/2016/09/JavaScript-Prototype-Function-defined.png)


 `Foo.prototype` ında  whoAmI isimli bir metod oluşturalım.

```javascript
Foo.prototype.whoAmI  =  function()  {
	return  "I am "  +  this.name;
}	
```

![enter image description here](http://www.javascripttutorial.net/wp-content/uploads/2016/09/JavaScript-Prototype-Function.png)

Foo prototipine referans olarak bağlanmış foo instance oluşturalım.
 ```javascript
 let a = new Foo('ozer')
 ```
 
 Javascript engine `a` isminde yeni bir obje yarattı ve bu objeyi prototip bağlantısıyla `Foo.prototype` a bağladı.
 
**Prototype Chain**
 ![enter image description here](http://www.javascripttutorial.net/wp-content/uploads/2016/09/JavaScript-Prototype-New-Object.png) 
 
`console.log(a.constructor.prototype)`

Şimdi de b isminde bir instance oluşturalım.
```javascript 
let b = new Foo('b');
```

![enter image description here](http://www.javascripttutorial.net/wp-content/uploads/2016/09/JavaScript-Prototype-second-object.png)

`b` objesine metod ekleyelim.

```javascript
b.say  =  function()  {
	console.log('Hi from '  +  this.whoAmI());
}

``` 
`Foo.prototype`  içerisinde görünmez. Direk `b` objesine tanımladık. b objesini genişlettik.

![enter image description here](http://www.javascripttutorial.net/wp-content/uploads/2016/09/JavaScript-Prototype-add-method-to-object.png)

***Karşılaştırmalar***
```javascript
a.whoAmI()
a.say()
b.whoAmI()
b.say()
```
***Ne oldu?***
1- a veya b objesinde  `whoAmI` yok. Constructor özellikler yok.  
2- JavaScript engine aşağıdan yukarıya doğru prototip zincirinde arama başlatıyor. 
3- JavaScript engine Foo fonksiyonunu döndürüyor.

 1. Cemil sende kalem var mı?
 2. yok
 3. Ahmet'te vardı
 4. Ahmet sende kalem var mı?
 5. Var.
 6. `return Ahmet.constructor.prototype.kalem`

```javascript
console.log(a.constructor  ===  Foo);  // true
```

```javascript
console.log(a.constructor);  // Foo fonksiyonu
console.log(Foo.prototype);
console.log(b.constructor.prototype);
```

Built-in properties farkı

```javascript
let empty = {};
console.log(empty.toString);
// → function toString(){…} constructor fonksiyonu gösterir Function.prototype.toString
console.log(empty.toString());
// → [object Object] Function.prototype.toString fonksiyon sonucu
console.log(empty)
// {} returns empty object
```

```[object Object]```  JavaScript objesinin string olarak temsil edildiği halidir.

```toString```  olarak gösterilen fonksiyon ise Object verisinin prototip özelliğidir. 

Birçok veritipinin kendine ait prototip değeri vardır. 

```javascript
Number.prototype
String.prototype
Function.prototype
Array.prototype
Object.prototype
```
***Not:*** javascript console da deneyip inceleyebilirsiniz.
```javascript
console.log(Object.getPrototypeOf({}) == Object.prototype);
console.log(Object.getPrototypeOf([]) == Array.prototype);
console.log(Object.getPrototypeOf('') == String.prototype);
console.log(Object.getPrototypeOf(1) == Number.prototype);
```

![enter image description here](http://7xph6d.com1.z0.glb.clouddn.com/JavaScript_prototype-chain.png)

```obj.__proto__.__proto__.__proto__.prop3``` vs  ```obj.prop3 ``` 

Eğer bir obje tanımlanmamış veya bulunamayan bir özellik (property) için istek alırsa, özelliği önce kendi prototipinde arar, eğer bulamazsa prototipinin prototipinde arar. Buluncaya kadar böyle devam eder. Bulamazsa undefined basar. 

Yukarıdan aşağıya doğru devam eder.
```console.log(obj) // __proto__ değerine bak.```

## Kaynaklar:
http://brianway.github.io/2017/05/04/Learning-Notes-Understanding-the-Weird-Parts-of-JavaScript/

https://hackernoon.com/understand-nodejs-javascript-object-inheritance-proto-prototype-class-9bd951700b29

http://dmitrysoshnikov.com/ecmascript/javascript-the-core/

http://www.javascripttutorial.net/javascript-prototype/

http://eloquentjavascript.net/06_object.html

https://kenneth-kin-lum.blogspot.com/2012/10/javascripts-pseudo-classical.html?showComment=1484288337339#c1393503225616140233

https://zipcon.net/~swhite/docs/computers/languages/object_oriented_JS/inheritance.html